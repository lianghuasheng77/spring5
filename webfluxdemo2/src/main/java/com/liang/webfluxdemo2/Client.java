package com.liang.webfluxdemo2;

import com.liang.webfluxdemo2.entity.User;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/18/15:15
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        //调用服务器地址
        WebClient webclient =WebClient.create("http://127.0.0.1:49962");
        //根据id查询
        String id="1";
        User block = webclient.get()
                .uri("/users/{id}", id)
                .accept(MediaType.APPLICATION_JSON).retrieve()
                .bodyToMono(User.class)
                .block();
        System.out.println(block.toString());

        //查询所有
        Flux<User> results=webclient.get()
                .uri("/users")
                .accept(MediaType.APPLICATION_JSON).retrieve()
                .bodyToFlux(User.class);
            results.map(stu->stu.getName())
                    .buffer()
                    .doOnNext(System.out::println)
                    .blockFirst();//block相当于订阅的部分 不订阅是取不出来的

    }
}
