package com.liang;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/20/10:20
 * @Description:
 */
public class Calculator {
    private static final Logger logger = LoggerFactory.getLogger(Calculator.class);
    public double divide(double a, double b) throws Exception {
        if (b == 0) {
            logger.error("被除数不能为0");
            throw new Exception("被除数是0");
        }
        return a / b;
    }
}
