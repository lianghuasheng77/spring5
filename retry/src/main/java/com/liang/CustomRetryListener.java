package com.liang;

import com.github.rholder.retry.Attempt;
import com.github.rholder.retry.RetryListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/20/10:16
 * @Description:
 */
public class CustomRetryListener implements RetryListener {
    private static final Logger logger = LoggerFactory.getLogger(CustomRetryListener.class);
    @Override
    public <V> void onRetry(Attempt<V> attempt) {
        // 第几次重试(注意:第一次重试其实是第一次调用)
        logger.info("retry time : [{}]", attempt.getAttemptNumber());

        // 距离第一次重试的延迟
        logger.info("retry delay : [{}]", attempt.getDelaySinceFirstAttempt());

        // 是否因异常终止
        if (attempt.hasException()) {
            logger.info("causeBy:", attempt.getExceptionCause());
        }
        // 正常返回时的结果
        if (attempt.hasResult()) {
            logger.info("result={}", attempt.getResult());
        }
    }
}
