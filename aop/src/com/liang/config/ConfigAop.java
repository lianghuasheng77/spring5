package com.liang.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/17/17:53
 * @Description:
 */
@Configuration
@ComponentScan(basePackages = {"com.liang"}) //开启注解扫描
@EnableAspectJAutoProxy(proxyTargetClass = true)//  <!-- 开启Aspect生成代理对象   --><aop:aspectj-autoproxy></aop:aspectj-autoproxy>
public class ConfigAop {
}
