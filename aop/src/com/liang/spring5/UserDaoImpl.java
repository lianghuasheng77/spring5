package com.liang.spring5;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/17/11:29
 * @Description:
 */
public class UserDaoImpl implements UserDao {
    @Override
    public int add(int a, int b) {
        System.out.println("add方法执行了。。。。。");
        return a + b;
    }

    @Override
    public String update(String id) {
        System.out.println("update方法执行了。。。。。");
        return id;
    }
}
