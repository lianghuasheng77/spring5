package com.liang.spring5;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/17/11:26
 * @Description:
 */
public interface UserDao {

    public int add(int a,int b);

    public String  update(String id);
}
