package com.liang.aopano;

import org.springframework.stereotype.Component;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/17/15:04
 * @Description: 被增强的一个类
 */
@Component
public class User {
    public void add() {
//        int i=10/0;
        System.out.println("add......");
    }
}
