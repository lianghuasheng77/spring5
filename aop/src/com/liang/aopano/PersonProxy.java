package com.liang.aopano;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/17/17:05
 * @Description:
 */
@Component
@Aspect //生成代理对象
@Order(1)
public class PersonProxy {


    //后置通知(返回通知)
    @Before(value = "execution(* com.liang.aopano.User.add(..))")
    public void afterReturning(){
        System.out.println("Person Before......");
    }
}
