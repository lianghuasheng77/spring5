package com.liang.aopano;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/17/15:07
 * @Description:  增强的类
 */
@Component
@Aspect //生成代理对象
@Order(3)
public class UserProxy {
    //相同切入点抽取
    @Pointcut(value = "execution(* com.liang.aopano.User.add(..))")
    public void pointdemo()
    {

    }
    //  前置通知
    // @Before注解表示前置通知
    @Before(value="pointdemo()")
    public void before(){
        System.out.println("before......");
    }
    //最终通知
    @After(value="execution(* com.liang.aopano.User.add(..))")
    public void after(){
        System.out.println("after......");
    }
    //后置通知 （返回通知） 方法失败不执行
    @AfterReturning(value="execution(* com.liang.aopano.User.add(..))")
    public void afterReturn(){
        System.out.println("AfterReturning......");
    }
    @AfterThrowing(value="execution(* com.liang.aopano.User.add(..))")
    public void afterThrowing(){
        System.out.println("AfterThrowing......");
    }
    //环绕通知
    @Around(value="execution(* com.liang.aopano.User.add(..))")
    public void around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("环绕之前......");
        //被增强的方法执行
        proceedingJoinPoint.proceed();
        System.out.println("环绕之后......");
    }
}
