package com.liang.test;

import com.liang.aopano.User;
import com.liang.aopxml.Book;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/17/17:49
 * @Description:
 */
public class TestAopXml {
    @Test
    public void testAopAnno(){
        ApplicationContext context= new ClassPathXmlApplicationContext("bean2.xml");
        Book book = context.getBean("book", Book.class);
        book.buy();
        /**
         * before......
         * buy......
         */
    }
}
