package com.liang.test;

import com.liang.aopano.User;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/17/16:09
 * @Description:
 */
public class TestAop {
    @Test
    public void testAopAnno(){
        ApplicationContext context= new ClassPathXmlApplicationContext("bean1.xml");
        User user = context.getBean("user", User.class);
        user.add();
        /**
         * before......
         * add......
         */
    }
}
