package com.liang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/21/14:47
 * @Description: @Link{We'll also need a standard Spring Boot main class to scan the beans and initialize the application:}
 */
@SpringBootApplication
public class MocksApplication {
    public static void main(String[] args) {

        SpringApplication.run(MocksApplication.class,args);
    }
}
