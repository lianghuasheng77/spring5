package com.liang.basic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/21/14:40
 * @Description:For @Link {this tutorial, the given classes return a single name regardless of the id provided.
 * This is done so that we don't get distracted by testing any complex logic.}
 */
@Service
public class UserService {
    private NameService nameService;

    @Autowired
    public UserService(NameService nameService) {
        this.nameService = nameService;
    }

    public String getUserName(String id) {
        return nameService.getUserName(id);
    }
}
