package com.liang.basic.config;

import com.liang.basic.service.NameService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/21/14:52
 * @Description:
 */
@Profile("test")// Spring 仅在“测试”配置文件处于活动状态时应用此配置。
@Configuration
public class NameServiceTestConfiguration {
    @Bean()
    @Primary//注释用于确保使用此实例而不是真实实例进行自动装配 该方法本身创建并返回NameService类的 Mockito 模拟。
    public NameService nameService() {
        return Mockito.mock(NameService.class);
    }
}
