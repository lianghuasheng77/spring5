package com.liang.test;

import com.liang.MocksApplication;
import com.liang.basic.service.NameService;
import com.liang.basic.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/21/15:06
 * @Description:
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = MocksApplication.class)//1.5还是1.6就被弃用了
@SpringBootTest(classes = MocksApplication.class)
public class UserServiceUnitTest {
    @Autowired
    private UserService userService;
    @Autowired
    private NameService nameService;
    @Test
    public void  whenUserIdIsProvided_thenRetrievedNameIsCorrect(){
        Mockito.when(nameService.getUserName("SomeId")).thenReturn("Mock user name");
        String testName=userService.getUserName("SomeId");
        Assert.assertEquals("Mock user name", testName);
    }
    /**
     * 在这个快速教程中，我们展示了将 Mockito 模拟注入 Spring Bean 是多么容易。
     */
}
