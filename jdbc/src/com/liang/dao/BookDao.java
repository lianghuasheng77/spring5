package com.liang.dao;

import com.liang.entity.Book;

import java.util.List;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/18/16:36
 * @Description:
 */
public interface BookDao {
    void add(Book book);

    void updateBook(Book book);

    void delete(String id);

    int selectCount();

    Book findBookInfo(String id);

    List<Book> findAll();

    //批量添加
    void batchAddBook(List<Object[]> batchArgs);
    //批量修改
    void batchUpdateBook(List<Object[]> batchArgs);
    //批量删除
    void batchDeleteBook(List<Object[]> batchArgs);
}
