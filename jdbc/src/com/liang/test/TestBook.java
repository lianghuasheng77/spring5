package com.liang.test;

import com.liang.entity.Book;
import com.liang.service.BookService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/18/17:33
 * @Description:
 */
public class TestBook {
    /**
     * 测试JdbcTemplate添加功能
     */
    @Test
    public void testJdbcTemplate() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);
        Book book = new Book();
        book.setUserId("1");
        book.setUsername("java");
        book.setUstatus("a");
        bookService.addBook(book);
    }

    /**
     * 测试修改
     */
    @Test
    public void test01() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);
        Book book = new Book();
        book.setUserId("1");
        book.setUsername("javahhhh");
        book.setUstatus("下班le");
        bookService.update(book);

    }

    /**
     * 测试删除
     */
    @Test
    public void test02() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);
        bookService.deleteBook("1");
    }
    /**
     * 测试查询
     */
    @Test
    public void test03() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);
        System.out.println(bookService.findCount());
    }
    /**
     * 测试查询返回对象
     */
    @Test
    public void test04() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);
        System.out.println(bookService.findOne("1"));
    }
    /**
     * 测试查询返回集合
     */
    @Test
    public void test05() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);
        System.out.println(bookService.findAll());
        //[Book{userId='1', username='dd', ustatus='s'}, Book{userId='2', username='sss', ustatus='aa'}]
    }
    /**
     * 测试批量添加
     */
    @Test
    public void test06() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);
        List<Object[]> batchArgs = new ArrayList<>();
        Object[] o1={"3","java","a"};
        Object[] o2={"4","java","b"};
        Object[] o3={"5","c","java"};
        batchArgs.add(o1);
        batchArgs.add(o2);
        batchArgs.add(o3);
        //调用批量添加方法
        bookService.batchAdd(batchArgs);
    }
    /**
     * 批量修改
     */
    @Test
    public void test07() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);
        List<Object[]> batchArgs = new ArrayList<>();
        Object[] o1={"java","a","3"};
        Object[] o2={"java","b","4"};
        Object[] o3={"mysql","c","5"};
        batchArgs.add(o1);
        batchArgs.add(o2);
        batchArgs.add(o3);
        bookService.batchUpdateBook(batchArgs);

    }
    /**
     * 批量删除
     */
    @Test
    public void test08() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);
        List<Object[]> batchArgs = new ArrayList<>();
        Object[] o1={"3"};
        Object[] o2={"4"};
        batchArgs.add(o1);
        batchArgs.add(o2);
        //调用方法实现批量修改
        bookService.batchDelete(batchArgs);
    }
}
