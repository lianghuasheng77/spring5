package com.liang.demoreactor8.reactor8;


import java.util.Observable;

/**
 * @Author: liangjy
 * @Date:2022/1/17 21:03
 * @Package_name: com.liang.demoreactor8.reactor8
 * @Description:
 */
public class ObserverDemo extends Observable {
    public static void main(String[] args) {
        ObserverDemo observer = new ObserverDemo();
        //添加观察者
        observer.addObserver((o, arg) -> {
            System.out.println("发生变化");
        });

        observer.addObserver((o, arg) -> {
            System.out.println("手动被观察者通知，准备改变");
        });
        /**
         * 监控
         */
        observer.setChanged();//监控到数据得到变化
        observer.notifyObservers();//通知
    }

}
