package com.liang.demoreactor8.reactor8;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * @Author: liangjy
 * @Date:2022/1/17 21:59
 * @Package_name: com.liang.demoreactor8.reactor8
 * @Description:
 */
public class TestReactor {
    public static void main(String[] args) {
       //just方法直接声明
        Flux.just(1,2,3,4).subscribe(System.out::println);
        Mono.just(1).subscribe(System.out::print);
//        //其他方法
//        Integer[] array={1,2,3,4};
//        Flux.fromArray(array);
//
//        List<Integer> list= Arrays.asList(array);
//        Flux.fromIterable(list);
//        Stream<Integer> stream=list.stream();
//        Flux.fromStream(stream);


    }
}
