package com.liang.spring5.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/17/9:22
 * @Description: 完全注解开发
 */
@Configuration  //作为配置类，替代xml配置文件
@ComponentScan(basePackages = {"com.liang"})//等价于xml<context:component-scan base-package="com.liang"></context:component-scan>
public class SpringConfig {

}
