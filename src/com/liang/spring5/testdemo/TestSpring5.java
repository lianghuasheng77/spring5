package com.liang.spring5.testdemo;

import com.liang.spring5.Book;
import com.liang.spring5.Orders;
import com.liang.spring5.User;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *@Author: ljy
 *@Date:2022/1/12 22:17
 *
 */

public class TestSpring5 {
  @Test
  public void testAdd(){
    //1、加载spring的配置文件 在src下
    ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
   // BeanFactory context = new ClassPathXmlApplicationContext("bean1.xml");
  //   ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("bean1.xml");

    //2、获取配置的对象
    User user = context.getBean("user", User.class);
    System.out.println(user);
    user.add();

  }
  @Test
  public void testBook1(){
    //加载配置文件
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean1.xml");
    //获取配置对象
    Book book = applicationContext.getBean("book", Book.class);
    System.out.println(book);
    book.testDemo();
  }
  @Test
  public void testOrsers(){
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean1.xml");
    Orders orders = applicationContext.getBean("orders", Orders.class);
    System.out.println(orders);
    orders.orderTest();
  }
}
