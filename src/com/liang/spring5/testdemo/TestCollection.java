package com.liang.spring5.testdemo;

import com.liang.spring5.collectiontype.Book;
import com.liang.spring5.collectiontype.Stu;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/13/17:04
 * @Description:
 */
public class TestCollection {
    @Test
    public void testCol() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean5.xml");
        Stu stu = applicationContext.getBean("stu", Stu.class);
        stu.test();
        /**
         * [java课程, 数据库课程]
         * [张三, 小三]
         * {JAVA=java, PHP=php}
         * [MySQL, Redis]
         */
        /**
         * [java课程, 数据库课程]
         * [张三, 小三]
         * {JAVA=java, PHP=php}
         * [MySQL, Redis]
         * [Course{cname='Spring5框架'}, Course{cname='Mybatis框架'}]
         */
    }

    /**
     * 模拟bean作用域是单实例还是多实例
     */
    @Test
    public void testCol2() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean6.xml");
        Book book = applicationContext.getBean("book", Book.class);
        Book book1 = applicationContext.getBean("book", Book.class);
//        book.test();
        System.out.println(book);
        System.out.println(book1);
        /**
         *[易筋经, 九阴真经, 九阳神功]
         * com.liang.spring5.collectiontype.Book@2fd66ad3
         * com.liang.spring5.collectiontype.Book@2fd66ad3
         *
         * com.liang.spring5.collectiontype.Book@2fd66ad3
         * com.liang.spring5.collectiontype.Book@5d11346a
         */
    }

}
