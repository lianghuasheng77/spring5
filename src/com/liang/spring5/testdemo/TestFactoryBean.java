package com.liang.spring5.testdemo;

import com.liang.spring5.collectiontype.Course;
import com.liang.spring5.factorybean.MyBean;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.applet.AppletContext;

/**
 * @Author: liangjy
 * @Date:2022/1/13 22:50
 * @Package_name: com.liang.spring5.testdemo
 * @Description:
 */
public class TestFactoryBean {
    /**
     * 该测试是MyBean未实现FactoryBean的
     */
    @Test
    public void testFac01() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean7.xml");
        MyBean myBean = applicationContext.getBean("myBean", MyBean.class);
        System.out.println(myBean);

    }

    /**
     * 该测试是MyBean实现FactoryBean的
     */
    @Test
    public void testFac02() {
        //相当于getObject 代理了getBean方法
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean7.xml");
        Course myBean = applicationContext.getBean("myBean", Course.class);
        System.out.println(myBean);
    }
}
