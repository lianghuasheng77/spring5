package com.liang.spring5.testdemo;

import com.liang.spring5.config.SpringConfig;
import com.liang.spring5.service.TestAnnoService;
import com.liang.spring5.service.TestDaoService;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.applet.AppletContext;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/14/16:10
 * @Description: 测试通过注解创建对象
 */
public class TestAnno {
    /**
     * 测试通过注解方式创建
     */
    @Test
    public void test01() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beanb.xml");
        TestAnnoService userService = applicationContext.getBean("userService", TestAnnoService.class);
        System.out.println(userService);
        userService.add();
        /**
         * TestAnnoService{}
         * service add ...
         */
    }

    /**
     * 测试@Autowired 注解
     */
    @Test
    public void test02() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beanb.xml");
        TestDaoService testDaoService = applicationContext.getBean("testDaoService", TestDaoService.class);
        System.out.println(testDaoService);
        testDaoService.add();
        /**
         * service add ......
         * dao add ......
         */

    }

    /**
     * 测试完全注解开发
     */
    @Test
    public void test03() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
        TestDaoService testDaoService = applicationContext.getBean("testDaoService", TestDaoService.class);
        System.out.println(testDaoService);
        testDaoService.add();
    }
    /**
     * ceshi
     * TestDaoService{name='abc', testDao=com.liang.spring5.dao.TestDaoImpl@76908cc0}
     * service add ......abc
     * dao add ......
     */
}
