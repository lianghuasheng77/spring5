package com.liang.spring5.testdemo;

import com.liang.spring5.bean.Emp;
import com.liang.spring5.bean.Orders;
import com.liang.spring5.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/13/14:51
 * @Description:
 */
public class TestBean {
    @Test
    public void test01() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean2.xml");
        UserService userService = applicationContext.getBean("userService", UserService.class);
        userService.add();

    }

    @Test
    public void test02() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean3.xml");
        Emp emp = applicationContext.getBean("emp", Emp.class);
        emp.add();
    }

    @Test
    public void test03() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean4.xml");
        Emp emp = applicationContext.getBean("emp", Emp.class);
        emp.add();
    }

    /**
     * 测试bean的生命周期 总共是七步
     */
    @Test
    public void test04() {
        AbstractApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean8.xml");
        Orders orders = applicationContext.getBean("orders", Orders.class);
        System.out.println("第四步，获取创建bean实例对象");
        System.out.println(orders);
//        applicationContext.close();


        //手动让bean实例销毁
        /**
         * 第一步 执行无参构造创建bean实例
         * 第二步 调用set方法设置属性值
         * 这是初始化之前执行的方法
         * 第三步 执行初始化的方法
         * 这是初始化之后执行的方法
         * 第四步，获取创建bean实例对象
         * Orders{oname='手机'}
         */
    }

    /**
     * 测试bean的autowire
     */
    @Test
    public void test05() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean9.xml");
        com.liang.spring5.autowire.Emp emp = applicationContext.getBean("emp", com.liang.spring5.autowire.Emp.class);
        System.out.println(emp);
        /**
         * Emp{dept=Dept{}}
         */
    }
}
