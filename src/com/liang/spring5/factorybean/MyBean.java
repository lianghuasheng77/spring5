package com.liang.spring5.factorybean;

import com.liang.spring5.collectiontype.Course;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.FactoryBean;

/**
 * @Author: ljy
 * @Date:2022/1/13 22:39
 * @Description:bean7.xml FactoryBean 你在Bean里面配置的类型可以和你返回的类型不一致
 */

public class MyBean implements FactoryBean<Course>{

    /**
     * 定义返回Bean
     *
     * @return
     * @throws Exception
     */
    @Override
    public Course getObject() throws Exception {
        Course course = new Course();
        course.setCname("abc");
        return course;

    }

    @Override
    public Class<?> getObjectType() {
        return null;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
}
