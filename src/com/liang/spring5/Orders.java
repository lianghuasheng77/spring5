package com.liang.spring5;

import java.security.PublicKey;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/13/10:53
 * @Description:
 */
public class Orders {
    private String oname;
    private String address;

    public Orders(String oname,String address){
        this.oname=oname;
        this.address=address;
    }
    public void orderTest(){
        System.out.println(oname+"::"+address);
    }

}
