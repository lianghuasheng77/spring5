package com.liang.spring5.autowire;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/14/8:54
 * @Description:
 */
public class Dept {

    @Override
    public String toString() {
        return "Dept{}";
    }
}
