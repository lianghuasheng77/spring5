package com.liang.spring5;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/13/10:05
 * @Description: 演示使用set方法进行注入属性
 */
public class Book {
    private String bname;
    private String bauthor;
    private String address;

    //set 注入属性
    public void setBname(String bname) {
        System.out.println("ceshi>>>>>");
        this.bname = bname;
    }

    public void setBauthor(String bauthor) {
        this.bauthor = bauthor;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void testDemo(){
        System.out.println(bname+"::"+bauthor+"::"+address);
    }

}
