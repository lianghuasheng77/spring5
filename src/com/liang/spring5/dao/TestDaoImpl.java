package com.liang.spring5.dao;

import org.springframework.stereotype.Repository;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/14/17:53
 * @Description:
 */
@Repository(value = "testDaoImpl1")
public class TestDaoImpl implements TestDao{
    @Override
    public void add() {
        System.out.println("dao add ......");
    }
}
