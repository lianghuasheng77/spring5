package com.liang.spring5.dao;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/13/14:18
 * @Description:
 */
public interface UserDao {

    public void update();
}
