package com.liang.spring5.dao;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/13/14:18
 * @Description:
 */
public class UserDaoImpl implements UserDao{
    @Override
    public void update() {
        System.out.println("dao update .....");
    }
}
