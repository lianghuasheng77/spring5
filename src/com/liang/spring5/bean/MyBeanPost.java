package com.liang.spring5.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;


/**
 * @Author: liangjy
 * @Date:2022/1/14 1:19
 * @Package_name: com.liang.spring5.bean
 * @Description: 只要实现 BeanPostProcessor 接口 spring 就把该类作为后置处理器
 * 那么将会为配置文件中所有的bean 实例加上 后置处理器
 */
public class MyBeanPost implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("这是初始化之前执行的方法");
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("这是初始化之后执行的方法");
        return bean;
    }

}
