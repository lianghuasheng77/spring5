package com.liang.spring5.bean;

/**
 * @Author: liangjy
 * @Date:2022/1/14 0:37
 * @Package_name: com.liang.spring5.bean
 * @Description:
 */
public class Orders {
    /**
     * 无参构造
     */
    public Orders() {
        System.out.println("第一步 执行无参构造创建bean实例");
    }

    private String oname;

    public void setOname(String oname) {
        this.oname = oname;
        System.out.println("第二步 调用set方法设置属性值");
    }

    //创建执行的初始化方法
    public void initMethod() {
        System.out.println("第三步 执行初始化的方法");
    }

    //创建执行的销毁的方法
    public void dectroyMethod() {
        System.out.println("第五步 执行销毁的方法");
    }

    @Override
    public String toString() {
        return "Orders{" +
                "oname='" + oname + '\'' +
                '}';
    }
}
