package com.liang.spring5.service;


import com.liang.spring5.dao.TestDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/14/17:57
 * @Description:
 */
@Service
public class TestDaoService {
    @Value(value="abc")
    private String name;

    //定义dao类对象
    //不需要添加set方法  里面已经封装了
//    @Autowired //根据类型进行注入
//    @Qualifier(value = "testDaoImpl1") //因为多态 指定具体的实现类 根据名称进行注入
//    private TestDao testDao;


//    @Resource //根据你的类型注入
    @Resource(name="testDaoImpl1") //根据名称进行注入
    private TestDao testDao;

    public void add() {
        System.out.println("service add ......"+name);
        testDao.add();
    }

//    @Override
//    public String toString() {
//        return "TestDaoService{" +
//                "testDao=" + testDao +
//                '}';
//    }

    @Override
    public String toString() {
        return "TestDaoService{" +
                "name='" + name + '\'' +
                ", testDao=" + testDao +
                '}';
    }
}
