package com.liang.spring5.service;

import com.liang.spring5.dao.UserDao;
import com.liang.spring5.dao.UserDaoImpl;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/13/14:14
 * @Description:
 */
public class UserService {
    //创建UserDao类型属性，生成set 方法
    private UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void add(){
        System.out.println("service add .......");

        //原始方式 ：创建UserDao对象 多态
//        UserDao userDao = new UserDaoImpl();
//        userDao.update();
        userDao.update();
    }


}
