package com.liang.spring5.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/14/13:59
 * @Description:
 */
//在注解里面value属性值可以省略不写
//默认值是类名称，首字母小写 testannoService
@Component(value = "userService") //<bean id="userService" class="..">
public class TestAnnoService {

    public void add() {
        System.out.println("service add ...");
    }

    @Override
    public String toString() {
        return "TestAnnoService{}";
    }
}
