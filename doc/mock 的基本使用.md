# mock 的基本使用

官网[tutorial](https://www.baeldung.com/mockito-annotations)快速入门【Junit+mockito模拟】

# 项目结构如下

![](.\images\微信截图_20220121155759.png)

## 1、Maven 依赖

我们需要以下 Maven 依赖项来进行单元测试和模拟对象：

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter</artifactId>
    <version>2.6.1</version>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
    <version>2.6.1</version>
    <scope>test</scope>
</dependency>
<dependency>
    <groupId>org.mockito</groupId>
    <artifactId>mockito-core</artifactId>
    <version>2.21.0</version>
</dependency>
```

## 2、编写测试

### 2.1**业务逻辑**

首先，让我们创建一个我们将要测试的简单服务：

```java
@Service
public class NameService {
    public String getUserName(String id) {
        return "Real user name";
    }
}
```

并将其注入到*UserService*类中：

```java
@Service
public class UserService {

    private NameService nameService;

    @Autowired
    public UserService(NameService nameService) {
        this.nameService = nameService;
    }

    public String getUserName(String id) {
        return nameService.getUserName(id);
    }
}
```

我们还需要一个标准的 Spring Boot 主类来扫描 bean 并初始化应用程序：

```java
@SpringBootApplication
public class MocksApplication {
    public static void main(String[] args) {
        SpringApplication.run(MocksApplication.class, args);
    }
}
```

### 2.2编写测试

```java
@Profile("test")
@Configuration
public class NameServiceTestConfiguration {
    @Bean
    @Primary
    public NameService nameService() {
        return Mockito.mock(NameService.class);
    }
}
```

> *@Profile*注释告诉 Spring 仅在“测试”配置文件处于活动状态时应用此配置。
>
> *@Primary*注释用于确保使用此实例而不是真实实例进行自动装配。*该方法本身创建并返回NameService*类的 Mockito 模拟。

现在我们可以编写单元测试：

```java
/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/21/15:06
 * @Description:
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MocksApplication.class)
public class UserServiceUnitTest {
    @Autowired
    private UserService userService;
    @Autowired
    private NameService nameService;

    @Test
    public void  whenUserIdIsProvided_thenRetrievedNameIsCorrect(){
        Mockito.when(nameService.getUserName("SomeId")).thenReturn("Mock user name");
        String testName=userService.getUserName("SomeId");
        Assert.assertEquals("Mock user name", testName);
    }
}
```

## 3、可能出现的错误

```java
bean could not be registered. A bean with that name has already been defined in xxx
```

由于`NameService`在两个地方均有注入，就会产生冲突 。解决如下：

- application.yml

```yaml
spring:
  main:
    allow-bean-definition-overriding: true
```

# 谢谢观看 ，菜鸟告辞~