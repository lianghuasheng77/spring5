# spring5 框架

## 1、spring概念

1. 是针对Bean的生命周期进行管理的轻量级容器 spring是轻量级的开源的javaEE框架；

2. 解决企业应用开发的复杂性；

3. spring有两个核心部分：IOC和AOP

   - IOC:控制反转，把创建对象过程交给spring进行
   - Aop:面向切面，不修改源代码进行功能增强、

4. spring特点：

   - 方便解耦，简化开发
   - Aop编程支持
   - 方便程序的测试 比如junit
   - 方便集成优秀的框架
   - 方便进行事务操作
   - 降低API开发

5. [spring下载地址版本选用的为5.2.6](https://repo.spring.io/release/org/springframework/spring/)

6. spring架构图

   ![](.\images\微信截图_20220112215142.png)

7.导入jar包![](.\images\微信截图_20220112220221.png)

8.创建普通类，在这个类创建普通方法

```java

public class User {
    public static void main(String[] args) {
        System.out.println("add........");
    }
}
```

9.创建Spring配置文件，在配置文件配置创建的对象

- spring配置文件使用xml

  ```xml
  <?xml version="1.0" encoding="UTF-8"?>
  <beans xmlns="http://www.springframework.org/schema/beans"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
  <!--配置User对象创建 bean 标签-->
      <bean id="user" class="com.liang.spring5.User"></bean>
  </beans>
  ```

10.进行测试代码编写

```java
public class TestSpring5 {
  @Test
  public void testAdd(){
    //1、加载spring的配置文件 在src下
    ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
    //2、获取配置的对象
    User user = context.getBean("user", User.class);
    System.out.println(user);
    user.add();

  }
}
```



## 2、IOC容器

### 1、IOC底层原理（概念和原理）

- 什么是ioc

  ```jsp
  1、控制反转：把对象创建和对象之间的调用过程，交给Spring进行管理
  2、使用IOC目的：为了耦合度降低
  3、做入门案例就是IOC实现的
  
  ```

- IOC底层原理

  - xml解析、工厂模式、反射

- 画图讲解底层原理

  ![](.\images\图1.png)

- IOC过程

  ![](.\images\图2.png)

### 2、IOC接口（`BeanFactory`)

- IOC思想基于IOC容器完成，IOC容器底层就是对象工厂

- Spring提供容器两种实现方式；（两个接口）

  - `BeanFactory`:`IOC`容器基本实现，是spring内部使用接口，不提供给开发人员进行使用

    **加载配置文件的时候并不会创建对象，在获取对象（使用）才去创建对象。**`懒汉式`

    ```java
    public class TestSpring5 {
      @Test
      public void testAdd(){
        //1、加载spring的配置文件 在src下
        BeanFactory context = new ClassPathXmlApplicationContext("bean1.xml");
       // ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("bean1.xml");
        //2、获取配置的对象
        User user = context.getBean("user", User.class);
        System.out.println(user);
        user.add();
    
      }
    }
    ```

    

  - `ApplicationContext` ：`BeanFactory`接口的子接口，提供更多更强大的功能，一般由开发人员进行使用 

    **加载配置文件的时候就会把在配置文件对象进行创建**`饿汉式`**用内存换速度（耗时耗力）**

    ```java
    public class TestSpring5 {
      @Test
      public void testAdd(){
        //1、加载spring的配置文件 在src下
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
       // ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("bean1.xml");
        //2、获取配置的对象
        User user = context.getBean("user", User.class);
        System.out.println(user);
        user.add();
    
      }
    }
    ```

> 上面两种接口具有一定的关联性 因为`ClassPathXmlApplicationContext`实现了`BeanFactory`,`ApplicationContext`这两个接口 类的继承如下图所示

![](.\images\微信截图_20220113085316.png)

#### 1、ApplicationContext接口有实现类 如图：

![idea快捷键 ctl+h](.\images\微信截图_20220113092231.png)

![](.\images\QQ截图20220117093250.png)

#### 2、BeanFactory的接口实现类如下：

![](.\images\微信截图_20220113093600.png)

### 3、IOC操作 Bean管理（基于xml）

#### 1什么是Bean管理

**Bean管理指的是两个操作**

##### 1、spring创建对象

##### 2、spring注入属性

#### 2、Bean管理操作有两种方式

##### 1、基于xml配置文件方式实现

- 基于xml方式创建`对象`

  ```xml
  <!--配置User对象创建 bean 标签-->
      <bean id="user" class="com.liang.spring5.User"></bean>
  ```

  在spring配置文件中，使用bean标签，标签里面添加对应属性，就可以实现对象创建。

  在bean标签里有很多属性，介绍常用的**属性**

  > id属性：给对象起一个别名和标识
  >
  > class 属性：要创建类的全路径
  >
  > name:id里面不能有下划线 name 可以

  **创建对象时候，默认也是执行无参数的构造方法完成对象创建**

- 基于xml方式注入`属性`

  ```xml
    <!--  set方法注入属性  -->
      <bean id="book" class="com.liang.spring5.Book">
          <!--     使用property完成属性注入   -->
          <property name="bname" value="易筋经"></property>
          <property name="bauthor" value="易中天"></property>
      </bean>
  ```

  - DI:依赖注入,就是注入属性

    1. `使用set方法进行注入`

       ```java
        private String bname;
        private String bauthor;
         //set 注入属性
           public void setBname(String bname) {
               this.bname = bname;
           }
       
           public void setBauthor(String bauthor) {
               this.bauthor = bauthor;
           }
       ```

       **基于bean方式的set注入**

       ```java
        @Test
         public void testBook1(){
           //加载配置文件
           ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean1.xml");
           //获取配置对象
           Book book = applicationContext.getBean("book", Book.class);
           System.out.println(book);
           book.testDemo();
         }
       
       //结果 com.liang.spring5.Book@3c73951
       //易筋经::易中天
       ```

       

    2. `有参构造注入`

       - 创建类，定义属性

         ```java
         public class Orders {
             private String oname;
             private String address;
         
             public Orders(String oname,String address){
                 this.oname=oname;
                 this.address=address;
             }
         }
         ```

       - 在spring配置文件中进行配置

       ```xml
        <!--  有参构造注入属性  -->
           <bean id="orders" class="com.liang.spring5.Orders">
           <constructor-arg name="oname" value="abc"></constructor-arg>
           <constructor-arg name="address" value="china"></constructor-arg>
           </bean>
       ```

       - 测试

         ```java
          @Test
           public void testOrsers(){
             ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean1.xml");
             Orders orders = applicationContext.getBean("orders", Orders.class);
             System.out.println(orders);
             orders.orderTest();
           }
         //结果com.liang.spring5.Orders@7c24b813
         //abc::china
         ```

    3. `对2的一种xml简化【了解】p名称空间注入`

       - 在约束里面添加`  xmlns:p="http://www.springframework.org/schema/p"`

       ```xml
       <?xml version="1.0" encoding="UTF-8"?>
       <beans xmlns="http://www.springframework.org/schema/beans"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xmlns:p="http://www.springframework.org/schema/p"
              xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd"></beans>
       
       ```

       - 进行属性注入，在`bean`标签里面进行

         ```xml
          <bean id="book" class="com.liang.spring5.Book" p:bname="九阳神功" p:bauthor="无名氏">
             </bean>
         ```

       - 结果

         ```txt
         com.liang.spring5.Book@1b2abca6
         九阳神功::无名氏
         ```

    4. IOC操作Bean管理（`xml注入其他类型属性`）

       - 字面量

         ```xml
         (1)null值
          <property name="address">
                     <null/>
                 </property>
         //效果：
         com.liang.spring5.Book@7c24b813
         易筋经::易中天::null
         (2)属性值包含特殊符号 使用cdata
          <property name="address" >
                     <value>
                         <![CDATA[<<南京>>]]>
                     </value>
                 </property>
         //效果 易筋经::易中天::<<南京>>
         ```

       - 注入属性外部bean

       ```xml
       1、创建两个类service类和dao类
       2、在service调用dao里面的方法
       3、在spring的配置文件进行配置
       <?xml version="1.0" encoding="UTF-8"?>
       <beans xmlns="http://www.springframework.org/schema/beans"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
           <!--service和dao对象创建-->
           <bean id="userService" class="com.liang.spring5.service.UserService">
               <!--   注入userDao对象
                    name属性： 类里面的属性名称
                    ref属性：创建userDao对象bean标签id值-->
               <property name="userDao" ref="userDao"></property>
           </bean>
       
           <bean id="userDao" class="com.liang.spring5.dao.UserDaoImpl"></bean>
       </beans>
       //测试效果：
       //service add .......
       //dao update .....
       
       ```

       - 注入属性-内部bean和级联

       ```xml
       1、一对多关系：部门和员工
       一个部门有多个员工，一个员工属于一个部门
       部门是一，员工是多
       2、在实体类之间表示一对多的关系，员工表示所属部门，使用对象类型表示关系
       3、在spring配置文件中进行配置
       <?xml version="1.0" encoding="UTF-8"?>
       <beans xmlns="http://www.springframework.org/schema/beans"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
       
       
           <!--   内部bean -->
           <bean id="emp" class="com.liang.spring5.bean.Emp">
               <!--  设置两个普通属性      -->
               <property name="ename" value="lucky"></property>
               <property name="gender" value="女"></property>
               <!-- 设置对象类型属性      -->
               <property name="dept" >
                   <bean id="dept" class="com.liang.spring5.bean.Dept">
                       <property name="dname" value="安保部"></property>
                   </bean>
               </property>
           </bean>
       </beans>
       //结果：
       lucky::女::Dept{dname='安保部'}
       ```

       - 注入属性-级联赋值

         ```xml
         1、写法一
         <?xml version="1.0" encoding="UTF-8"?>
         <beans xmlns="http://www.springframework.org/schema/beans"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
             <!--   内部bean -->
             <bean id="emp" class="com.liang.spring5.bean.Emp">
                 <!--  设置两个普通属性      -->
                 <property name="ename" value="lucky"></property>
                 <property name="gender" value="女"></property>
                 <!--  级联赋值      -->
                 <property name="dept" ref="dept"></property>
             </bean>
             <bean id="dept" class="com.liang.spring5.bean.Dept">
                 <property name="dname" value="财务部"></property>
             </bean>
         </beans>
         //结果
         // lucky::女::Dept{dname='财务部'}
         2、另外一种写法：
         <?xml version="1.0" encoding="UTF-8"?>
         <beans xmlns="http://www.springframework.org/schema/beans"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
             <!--   内部bean -->
             <bean id="emp" class="com.liang.spring5.bean.Emp">
                 <!--  设置两个普通属性      -->
                 <property name="ename" value="lucky"></property>
                 <property name="gender" value="女"></property>
                 <!--  级联赋值      -->
                 <property name="dept" ref="dept"></property>
                 <!--    在实体类类里面得生成Dept get方法    -->
                 <property name="dept.dname" value="技术部"></property>
             </bean>
             <bean id="dept" class="com.liang.spring5.bean.Dept">
                 <property name="dname" value="财务部"></property>
             </bean>
         </beans>
         //结果 技术部会把财务部覆盖了
         ```

         - IOC操作Bean管理 （xml）注入集合属性

           ```xml
           1、注入数组类型属性
           
           2、注入List集合累型属性
           
           3、注入Map集合累型属性
           4、在spring配置内进行集合属性注入
           <?xml version="1.0" encoding="UTF-8"?>
           <beans xmlns="http://www.springframework.org/schema/beans"
                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                  xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
               <!--  集合类型属性注入 -->
               <bean id="stu" class="com.liang.spring5.collectiontype.Stu">
                   <!--   数组类型属性注入     -->
                   <property name="courses">
                       <array>
                           <value>java课程</value>
                           <value>数据库课程</value>
                       </array>
                   </property>
                   <!--  list类型属性注入 -->
                   <property name="list">
                       <list>
                           <value>张三</value>
                           <value>小三</value>
                       </list>
                   </property>
                   <!-- map类型属性注入       -->
                   <property name="maps">
                       <map>
                           <entry key="JAVA" value="java"></entry>
                           <entry key="PHP" value="php"></entry>
                       </map>
                   </property>
                   <!--  set类型属性注入       -->
                   <property name="sets">
                       <set>
                           <value>MySQL</value>
                           <value>Redis</value>
                       </set>
                   </property>
               </bean>
           </beans>
           //结果[java课程, 数据库课程]
           [张三, 小三]
           {JAVA=java, PHP=php}
           [MySQL, Redis]
           ```

           > 卖个关子 有两个点第一bean内部的集合外面的类无法引用可以做个公共抽取 第二类型都是基本类型 尝试下引用类型

         - IOC操作Bean管理 （xml）注入高级集合属性

           ```xml
           1、在集合里面设置对象类型值
            <!--    注入list集合类型，值是对象    -->
                   <property name="courseList">
                       <list>
                           <ref bean="course1"></ref>
                           <ref bean="course2"></ref>
                       </list>
                   </property>
            <!--  创建多个course对象  -->
               <bean id="course1" class="com.liang.spring5.collectiontype.Course">
                   <property name="cname" value="Spring5框架"></property>
               </bean>
               <bean id="course2" class="com.liang.spring5.collectiontype.Course">
                   <property name="cname" value="Mybatis框架"></property>
               </bean>
           2、把集合注入部分提取出来
           	a)在spring配置文件中引入名称空间 util
           <?xml version="1.0" encoding="UTF-8"?>
           <beans xmlns="http://www.springframework.org/schema/beans"
                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                  xmlns:p="http://www.springframework.org/schema/p"
                  xmlns:util="http://www.springframework.org/schema/util"
                  xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                                      http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd">
           </beans>
           	b)使用util标签完成list集合注入抽取
           <!--  提取list集合类型属性注入  -->
               <util:list id="bookList">
                   <value>易筋经</value>
                   <value>九阴真经</value>
                   <value>九阳神功</value>
               </util:list>
               <!--提取list集合类型属性注入使用-->
               <bean id="book" class="com.liang.spring5.collectiontype.Book">
                   <property name="list" ref="bookList"></property>
               </bean>
           //测试结果 [易筋经, 九阴真经, 九阳神功]
           ```

           - ioc操作Bean管理（`FactoryBean`）
           
             ```java
             1、Spring有两种类型bean,一种普通bean，另外一种工厂bean（FactoryBean）
             
             2、普通bean
              <!--提取list集合类型属性注入使用-->
                 <bean id="book" class="com.liang.spring5.collectiontype.Book">
                     <property name="list" ref="bookList"></property>
                 </bean>
             3、工厂bean:在配置文件定义bean类型可以和返回类型不一样
             a)创建类，让这个类作为工厂bean，实现接口FactoryBean.
             b)实现接口里面的方法，在实现的方法中定义返回bean的类型
             c)public class MyBean implements FactoryBean<Course> {
             
                 /**
                  * 定义返回Bean
                  *
                  * @return
                  * @throws Exception
                  */
                 @Override
                 public Course getObject() throws Exception {
                     Course course = new Course();
                     course.setCname("abc");
                     return course;
                 }
             
                 @Override
                 public Class<?> getObjectType() {
                     return null;
                 }
             
                 @Override
                 public boolean isSingleton() {
                     return false;
                 }
             }
              /**
                  * 该测试是MyBean实现FactoryBean的
                  */
                 @Test
                 public void testFac02() {
                     ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean7.xml");
                     Course myBean = applicationContext.getBean("myBean", Course.class);
                     System.out.println(myBean);
                 }
             ```
           
             - IOC操作Bean管理（bean作用域）
           
               ```java
               1、在Spring里面，设置创建bean实例是单实例还是多实例
               2、在Spring里面，默认情况下，bean是单实例对象
                  /**
                    * 模拟bean作用域是单实例还是多实例
                    */
                   @Test
                   public void testCol2() {
                       ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean6.xml");
                       Book book = applicationContext.getBean("book", Book.class);
                       Book book1 = applicationContext.getBean("book", Book.class);
               //        book.test();
                       System.out.println(book);
                       System.out.println(book1);
                       /**
                        *[易筋经, 九阴真经, 九阳神功]
                        * com.liang.spring5.collectiontype.Book@2fd66ad3
                        * com.liang.spring5.collectiontype.Book@2fd66ad3
                        */
               3、如何设置单实例还是多实例
               a) 在spring配置文件bean标签里面有属性(scope)用于设置单实例 
               b) scope属性值
               第一个值 默认值 singleton ,表示单实例对象
               第二个值 prototype ,表示是多实例对象
                <bean id="book" class="com.liang.spring5.collectiontype.Book" scope="prototype">
                       <property name="list" ref="bookList"></property>
                   </bean>
                /**
                    * 模拟bean作用域是单实例还是多实例
                    */
                   @Test
                   public void testCol2() {
                       ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean6.xml");
                       Book book = applicationContext.getBean("book", Book.class);
                       Book book1 = applicationContext.getBean("book", Book.class);
               //        book.test();
                       System.out.println(book);
                       System.out.println(book1);
                       /**
                        *[易筋经, 九阴真经, 九阳神功]
                        * 
                        * com.liang.spring5.collectiontype.Book@2fd66ad3
                        * com.liang.spring5.collectiontype.Book@5d11346a
                        */
                  c)singleton 和 prototype的区别
                  第一singleton单实例，prototype 多实例
                  第二设置scope的值是singleton时候 ，加载spring配置文件时候就会创建单实例对象
                  	   设置scope值是prototype时候，不是在加载spring配置文件时候创建对象，在调用
                  	   getBean方法时候创建多实例。
               ```
           
               - IOC操作Bean管理 （bean生命周期）
           
                 ```java
                 1、生命周期
                 a)从对象创建到对象销毁的过程
                 2、bean生命周期
                 a)通过构造器创建bean实例（无参数构造）
                 b)为bean的属性设置值和对其他bean引用（调用set方法）
                 把bean的实例传递bean后置处理器方法 postProcessBeforeInitialization
                 c)调用bean的初始化方法（需要进行配置）
                 把bean实例传递给bean后置处理器方法 postProcessAfterInitialization
                 d)bean可以使用了（对象获取到了）
                 e)当容器关闭时候，调用bean的销毁方法（需要进行配置销毁的方法）
                 3、演示bean的生命周期
                 <bean id="orders" class="com.liang.spring5.bean.Orders" init-method="initMethod" destroy-method="dectroyMethod">
                         <property name="oname" value="手机"></property>
                     </bean>
                 public class Orders {
                     /**
                      * 无参构造
                      */
                     public Orders() {
                         System.out.println("第一步 执行无参构造创建bean实例");
                     }
                 
                     private String oname;
                 
                     public void setOname(String oname) {
                         this.oname = oname;
                         System.out.println("第二步 调用set方法设置属性值");
                     }
                 
                     //创建执行的初始化方法
                     public void initMethod() {
                         System.out.println("第三步 执行初始化的方法");
                     }
                 
                     //创建执行的销毁的方法
                     public void dectroyMethod() {
                         System.out.println("第五步 执行销毁的方法");
                     }
                 
                     @Override
                     public String toString() {
                         return "Orders{" +
                                 "oname='" + oname + '\'' +
                                 '}';
                     }
                 }
                  /**
                      * 测试bean的生命周期
                      */
                     @Test
                     public void test04() {
                         ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean8.xml");
                         Orders orders = applicationContext.getBean("orders", Orders.class);
                         System.out.println("第四步，获取创建bean实例对象");
                         System.out.println(orders);
                 
                         //手动让bean实例销毁
                     }
                     //第一步 执行无参构造创建bean实例
                 	//第二步 调用set方法设置属性值
                     //第三步 执行初始化的方法
                     //第四步，获取创建bean实例对象
                     //Orders{oname='手机'}
                     
                 4、bean的后置处理器 bean 的生命周期 就多两步
                 5、演示添加后置处理器效果
                 a)创建类，实现接口BeanPostProcessor,创建后置处理器
                 /**
                 * @Description: 只要实现 BeanPostProcessor 接口 spring 就把该类作为后置处理器 那么将会为配置文件中所有的bean 实例加上 后置处理器
                 */
                 public class MyBeanPost implements BeanPostProcessor {
                     @Override
                     public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
                         System.out.println("这是初始化之前执行的方法");
                         return bean;
                     }
                 
                     @Override
                     public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
                         System.out.println("这是初始化之后执行的方法");
                         return bean;
                     }
                 
                 }
                 b)配置bean
                 <!--  配置 后置处理器  -->
                     <bean id="myBeanPost" class="com.liang.spring5.bean.MyBeanPost"></bean>
                 ```
           
             
             - IOC操作Bean管理（xml自动装配）
             
               ```xml
               1、什么是自动装配
               a)根据指定装配规则（属性名称或者属性类型），spring自动将匹配的属性值进行注入
                实现自动装配：
                       bean标签属性autowire,配置自动装配
                       autowire属性常用两个值：
                           byName根据属性名称注入，注入bean的id值和类属性名称一样
                           byType根据属性类型注入
                 byname:
                 <bean id="emp" class="com.liang.spring5.autowire.Emp" autowire="byName">
                   </bean>
                   <bean id="dept" class="com.liang.spring5.autowire.Dept"></bean>
               bytype:
               <bean id="emp" class="com.liang.spring5.autowire.Emp" autowire="byType">
                   </bean>
                   <bean id="dept" class="com.liang.spring5.autowire.Dept"></bean>
               ps:跟据类型创建的话存在小问题 同类型有多个bean
               2、演示自动装配过程
               ```
             
             - Ioc操作Bean管理（外部属性文件）
             
             ```xml
             1、直接配置数据库信息
             a)配置连接池 druid
             b)引入druid连接池依赖jar包
              <!--  直接配置连接池  -->
                 <bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource">
                     <property name="driverClassName" value="com.mysql.jdbc.Driver"></property>
                     <property name="url" value="jdbc:mysql://localhost:3306/seckill"></property>
                     <property name="username" value="root"></property>
                     <property name="password" value="123456"></property>
                 </bean>
             2、通过引入外部属性文件配置数据库连接池
             a)
             prop.driverClass=com.mysql.jdbc.Driver
             prop.url=jdbc:mysql://localhost:3306/seckill
             prop.userName=root
             prop.password=123456
             b)把外部properties属性文件引入到spring配置文件中。
             引入context命名空间
             <?xml version="1.0" encoding="UTF-8"?>
             <beans xmlns="http://www.springframework.org/schema/beans"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xmlns:context="http://www.springframework.org/schema/context"
                    xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                                        http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd ">
             </beans>
             c)在spring配置文件使用标签引入外部属性
             <context:property-placeholder location="classpath:jdbc.properties"/>
                     <bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource">
                         <property name="driverClassName" value="${prop.driverClass}"></property>
                         <property name="url" value="${prop.url}"></property>
                         <property name="username" value="${prop.userName}"></property>
                         <property name="password" value="${prop.password}"></property>
                     </bean>
             ```
             
             

##### 2、基于注解方式实现

###### 1、什么是注解

```java
1、注解时代码特殊标记，格式：@注解名称（属性名称=属性值，属性名称=属性值）
2、使用注解，注解作用在类上面，方法上面，属性上面
3、使用注解目的：为了简化xml配置
```

###### 2、Spring针对Bean管理中创建对象提供注解

```java
1、@Component 
2、@Service
3、@Controller
4、@Repository
上面四个注解功能是一样的，都可以用来创建bean实例
```

###### 3、基于注解方式实现对象创建

```xml
第一步：引入依赖 spring-aop
第二步：开启组件扫描
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd ">

    <!--  开启组件扫描
     1、如果扫描多个包，多个包使用逗号隔开
     2、或者写包上层目录
     -->
<!--    <context:component-scan base-package="com.liang.spring5.dao,com.liang.spring5.service"></context:component-scan>-->
    <context:component-scan base-package="com.liang"></context:component-scan>
</beans>
第三步、创建类，在类上面添加创建对象注解
//在注解里面value属性值可以省略不写
//默认值是类名称，首字母小写 testannoService
@Component(value = "userService") //<bean id="userService" class="..">
public class TestAnnoService {

    public void add() {
        System.out.println("service add ...");
    }
}
     /**
     * 测试通过注解方式创建
     */
    @Test
    public void test01() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beanb.xml");
        TestAnnoService userService = applicationContext.getBean("userService", TestAnnoService.class);
        System.out.println(userService);
        userService.add();
        /**
         * TestAnnoService{}
         * service add ...
         */
    }
```

###### 4、Bean管理注解方式-组件扫描配置

```xml
 <!--  示例1
    use-default-filters="false" 表示现在不使用默认filter，自己配置filter
    context:include-filter，设置扫描哪些内容 示例1代表只扫描带Controller注解的类
     -->
    <context:component-scan base-package="com.liang" use-default-filters="false">
        <context:include-filter type="annotation"
                                expression="org.springframework.stereotype.Controller"/>
    </context:component-scan>
 <!--  示例2
        下面配置扫描包所有内容
        context:exclude-filter：设置哪些内容不进行扫描
        除了controller 其他注解全都扫描
    -->
    <context:component-scan base-package="com.liang">
        <context:exclude-filter type="annotation"
                               expression="org.springframework.stereotype.Controller"/>
    </context:component-scan>
```

###### 5、基于注解方式实现属性注入(AutoWire)[几个注解的说明](https://mp.weixin.qq.com/s/INfq7enP5zrt8XydBcMWNw)

```java
1、@AutoWired:根据属性类型进行自动装配
第一步：把service和dao对象创建，在service和dao类添加创建对象注解
第二步：在service里面注入dao的对象，在service类中添加dao类型属性，在属性上面使用注解
@Service
public class TestDaoService {

    //定义dao类对象
    //不需要添加set方法  里面已经封装了
    @Autowired//根据类型进行注入
    private TestDao testDao;

    public void add() {
        System.out.println("service add ......");
    }
}
 /**
     * 测试@Autowired 注解
     
     */
    @Test
    public void test02() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beanb.xml");
        TestDaoService testDaoService = applicationContext.getBean("testDaoService", TestDaoService.class);
        System.out.println(testDaoService);
        testDaoService.add();
        /**
         * service add ......
         * dao add ......
         */
    }
2、@Qualifier：根据属性名称进行注入
这个@Qualifier的使用，和上面@Autowired一起使用
 //定义dao类对象
    //不需要添加set方法  里面已经封装了
    @Autowired //根据类型进行注入
    @Qualifier(value = "testDaoImpl1") //因为多态 指定具体的实现类 根据名称进行注入
    private TestDao testDao;
3、@Resource：可以根据类型注入，可以根据名称注入
//    @Resource //根据你的类型注入
    @Resource(name="testDaoImpl1") //根据名称进行注入
    private TestDao testDao;
注意像Autowired 和 Qualifier都是spring包下的：
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
而 Resource 是java的扩展包
import javax.annotation.Resource;
4、@Value:注入普通类型属性
 @Value(value="abc")
    private String name;


```

###### 6、完全注解开发

```java
1、创建配置类，替代xml的配置文件
@Configuration  //作为配置类，替代xml配置文件
@ComponentScan(basePackages = {"com.liang"})//等价于xml<context:component-scan base-package="com.liang"></context:component-scan>
public class SpringConfig {  
}
2、编写测试类
 /**
     * 测试完全注解开发
     */
    @Test
    public void test03() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
        TestDaoService testDaoService = applicationContext.getBean("testDaoService", TestDaoService.class);
        System.out.println(testDaoService);
        testDaoService.add();
    }
    /**
     * ceshi
     * TestDaoService{name='abc', testDao=com.liang.spring5.dao.TestDaoImpl@76908cc0}
     * service add ......abc
     * dao add ......
     */

```

## 3、AOP

### 1AOP基本概念

#### 1、什么是AOP

`面向切面编程（方面）`，利用AOP可以对业务逻辑的各个部分进行隔离，从而使得业务逻辑各部分之间的`耦合度`降低，提高程序的可重用性，同时提高了开发的效率。

![](.\images\图3.png)

通俗描述：不通过修改源代码方式，在主干功能里面添加新功能

使用登录的例子说明AOP

#### 2、底层原理

##### 1、AOP底层使用动态代理

有两种情况动态代理

![](.\images\图4.png)

**第一种 有接口情况** ，使用`JDK`动态代理



**第二种 没有接口情况**，使用`CGLIB`动态

##### 2、AOP（JDK动态代理）

```java
1、使用JDK动态代理，使用Proxy类里面的方法创建代理对象
java.lang.Object
	java.lang.reflect.Proxy
```

![](.\images\微信截图_20220117105835.png)

```java
调用newProxyInstance方法
方法有三个参数：
第一：类加载器
第二：增强方法所在的类，这个类实现的接口，支持多个接口
第三：实现这个接口InvocationHandler,创建代理对象，写增强的部分
2、编写JDK动态代理代码
a)创建接口，定义方法
public interface UserDao {

    public int add(int a,int b);

    public String  update(String id);
}
b)创建接口实现类，实现方法
public class UserDaoImpl implements UserDao{
    @Override
    public int add(int a, int b) {
        return a+b;
    }
    @Override
    public String update(String id) {
        return id;
    }
}
c)使用Proxy类创建接口代理对象
public class JDKProxy {
    public static void main(String[] args) {
        //创建接口实现类的代理对象
        Class[] interfaces = {UserDao.class};
        UserDaoImpl userDao = new UserDaoImpl();
//        Proxy.newProxyInstance(JDKProxy.class.getClassLoader(), interfaces, new InvocationHandler() {
//            @Override
//            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//                return null;
//            }
//        });//匿名内部类
        UserDao dao = (UserDao) Proxy.newProxyInstance(JDKProxy.class.getClassLoader(), interfaces, new UserDaoProxy(userDao));
        int result = dao.add(1, 2);
        System.out.println("result:".concat(String.valueOf(result)));

        System.out.println(JDKProxy.class.getClassLoader().toString());
    }
}
//创建代理对象代码
class UserDaoProxy implements InvocationHandler {
    private Object obj;
    //1、把创建的是谁的代理对象，把谁传递过来 此例应该把 UserDaoImpl 传递过来
    //有参数构造传递
    public UserDaoProxy(Object obj) {
        this.obj = obj;
    }
    //写增强的逻辑
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //方法之前
        System.out.println(">>>>>>>方法前" + method.getName() + ": 传递的参数" + Arrays.toString(args));
        //被增强的方法执行
        Object res = method.invoke(obj, args);

        //方法之后
        System.out.println(">>>>>>>>方法后" + obj);
        return res;
    }
    /**
     * 测试结果
     * >>>>>>>方法前add: 传递的参数[1, 2]
     * add方法执行了。。。。。
     * >>>>>>>>方法后com.liang.spring5.UserDaoImpl@6e0be858
     * result:3
     * sun.misc.Launcher$AppClassLoader@18b4aac2
     *
     * Process finished with exit code 0
     */
}
  
  
```

#### 3、AOP(术语)

![](.\images\图5.png)

```java
1、连接点
类里面哪些方法可以被增强，这些方法称为连接点

2、切入点
实际被真正增强的方法，称为切入点

3、通知(增强)
    a)实际增强的逻辑部分称为通知（增强）
    b)通知有多种类型(5种)
    	前置通知
    	后置通知
    	环绕通知
    	异常通知
    	最终通知 finally
    
4、切面
是动作
a)把通知应用到切入点的过程
```

#### 4、AOP操作-准备工作

```
1、Spring框架一般基于AspectJ实现AOP操作

```

##### 1、什么是AspectJ

```
a)AspectJ不是Spring组成部分，独立AOP框架，一般把AspectJ和Spring框架一起使用，进行AOP操作
```

##### 2、基于AspectJ实现AOP操作 

```
a)基于xml配置文件实现
b)基于注解方式实现（使用）

```

##### 3、在项目工程里面引入AOP相关依赖

![](.\images\QQ截图20220117144054.png)

##### 4、切入点表达式

```java
a)切入点表达式作用：知道对哪个类里面的哪个方法进行增强
b)语法结构：
execution([权限修饰符] [返回类型] [类全路径] [方法名称]([参数列表]))
举例1：对com.liang.dao.BookDao类里面的add进行增强
execution(* com.liang.dao.BookDao.add(..))
举例2：对com.liang.dao.BookDao类里面的所有方法进行增强
execution(* com.liang.dao.BookDao.*(..))
举例3：对com.liang.dao包里面所有类，类里面所有方法进行增强
execution(* com.liang.dao.*.*(..))
```

#### 5、AOP操作-AspectJ注解

```java
1、创建类，在类里面定义方法
public class User {
    public void add() {
        System.out.println("add......");
    }
}
2、创建增强类(编写增强逻辑)
    a)在增强类里面，创建方法，让不同方法代表不同通知类型
    public class UserProxy {
    //  前置通知
    public void before(){
        System.out.println("before......");
    }
}
3、进行通知的配置
a)在spring配置文件中,开启注解扫描
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
                           http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd ">
    <!-- 开启注解扫描       -->
    <context:component-scan base-package="com.liang.aopano"></context:component-scan>
</beans>
b)使用注解创建User和UserProxy
@Component
public class UserProxy {
    //  前置通知
    public void before(){
        System.out.println("before......");
    }
}
c)在增强类上面添加注解@Aspect
@Component
@Aspect //生成代理对象
public class UserProxy {
    //  前置通知
    public void before(){
        System.out.println("before......");
    }
}
d)在spring配置文件中开启生成代理对象
  <!-- 开启Aspect生成代理对象   -->
    <aop:aspectj-autoproxy></aop:aspectj-autoproxy>
4、配置不同类型的通知
a)在增强类的里面，在作为通知方法上面添加通知类型注解，使用切入点表达式配置
@Component
@Aspect //生成代理对象
public class UserProxy {
    //  前置通知
    // @Before注解表示前置通知
    @Before(value="execution(* com.liang.aopano.User.add(..))")
    public void before(){
        System.out.println("before......");
    }
   
}
b)
    @Component
@Aspect //生成代理对象
public class UserProxy {
    //  前置通知
    // @Before注解表示前置通知
    @Before(value="execution(* com.liang.aopano.User.add(..))")
    public void before(){
        System.out.println("before......");
    }
    //最终通知
    @After(value="execution(* com.liang.aopano.User.add(..))")
    public void after(){
        System.out.println("after......");
    }
    //后置通知 （返回通知） 方法失败不执行
    @AfterReturning(value="execution(* com.liang.aopano.User.add(..))")
    public void afterReturn(){
        System.out.println("AfterReturning......");
    }
    @AfterThrowing(value="execution(* com.liang.aopano.User.add(..))")
    public void afterThrowing(){
        System.out.println("AfterThrowing......");
    }
    //环绕通知
    @Around(value="execution(* com.liang.aopano.User.add(..))")
    public void around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("环绕之前......");
        //被增强的方法执行
        proceedingJoinPoint.proceed();
        System.out.println("环绕之后......");
    }
}
5、相同切入点抽取
  //相同切入点抽取
    @Pointcut(value = "execution(* com.liang.aopano.User.add(..))")
    public void pointdemo()
    {

    }
    //  前置通知
    // @Before注解表示前置通知
    @Before(value="pointdemo()")
    public void before(){
        System.out.println("before......");
    }
6、有多个增强类多对同一个方法进行增强，设置增强类优先级

a)在增强类上添加注解@Order(数字类型值)，数字类型值越小优先级越高
@Component
@Aspect //生成代理对象
@Order(1)
public class PersonProxy {
    //后置通知(返回通知)
    @Before(value = "execution(* com.liang.aopano.User.add(..))")
    public void afterReturning(){
        System.out.println("Person Before......");
    }
}
7、完全使用注解开发
a)创建配置类，不需要创建xml配置文件
@Configuration
@ComponentScan(basePackages = {"com.liang"}) //开启注解扫描
@EnableAspectJAutoProxy(proxyTargetClass = true)//  <!-- 开启Aspect生成代理对象   --><aop:aspectj-autoproxy></aop:aspectj-autoproxy>
public class ConfigAop {
}

```

#### 6、AOP操作（AspectJ配置文件）

```java
1、创建两个类，增强类和被增强类，创建方法

2、在spring配置文件中创建两个类的对象
 <!-- 创建对象  -->
    <bean id="book" class="com.liang.aopxml.Book"></bean>
    <bean id="bookProxy" class="com.liang.aopxml.BookProxy"></bean>
3、在spring配置文件中配置切入点
 <!--   配置aop增强-->
    <aop:config>
        <!--  切入点      -->
        <aop:pointcut id="p" expression="execution(* com.liang.aopxml.Book.buy(..))"/>
        <!--    配置切面    -->
        <aop:aspect ref="bookProxy">
            <!--    增强作用在具体的方法上        -->
            <aop:before method="before" pointcut-ref="p"/>
        </aop:aspect>
    </aop:config>
```



## 4、JDBCTemplate

### 1、什么是JdbcTemplate

```bash
Spring框架对JDBC进行封装，使用JdbcTemplate方便实现对数据库操作

```

### 2、准备工作

- 引入相关jar包

![](.\images\微信截图_20220118160307.png)

- 在spring配置文件配置数据库连接池

```xml
 <bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource"
          destroy-method="close">
        <property name="url" value="jdbc:mysql:///user_db" />
        <property name="username" value="root" />
        <property name="password" value="123456" />
        <property name="driverClassName" value="com.mysql.jdbc.Driver" />
    </bean>
```

- 配置JdbcTemplate对象，注入DataSource

```xml
 <!--    JdbcTemplate对象-->
    <bean id="jdbcTemplate" class="org.springframework.jdbc.core.JdbcTemplate">
        <!--    注入dataSource    -->
        <property name="dataSource" ref="dataSource"></property>
    </bean>
```

- 创建`service`类，常见`dao`类，在`dao`注入`jdbcTemplate`对象

```xml
配置文件
 <!--  开启组件的扫描  -->
    <context:component-scan base-package="com.liang"></context:component-scan>
Service:
@Service
public class BookService {
    //注入dao
    @Autowired
    private BookDao bookDao;
}
Dao:
@Repository
public class BookDaoImpl  implements  BookDao{

    // 注入JdbcTemplate
    @Autowired
    private JdbcTemplate jdbcTemplate;
}
```

### 3、JdbcTemplate操作数据库 -添加操作

```java
@Repository
public class BookDaoImpl implements BookDao {

    // 注入JdbcTemplate
    @Autowired
    private JdbcTemplate jdbcTemplate;

    //添加的方法
    @Override
    public void add(Book book) {
        //1、创建sql语句
        String sql = "insert into book values(?,?,?)";
        //2、调用方法实现

        Object[] args = {book.getUserId(), book.getUsername(), book.getUstatus()};
        int update = jdbcTemplate.update(sql, args);
//        int update = jdbcTemplate.update(sql, book.getUserId(), book.getUsername(), book.getUstatus());
        System.out.println(update);


    }
```

### 4、jdbcTemplate操作数据库

#### 查询返回某个值

```java
1、查询表里面有多少条记录，返回是某个值
2、使用JdbcTemplate实现查询返回某个值
 @Override
    public int selectCount() {
        String sql="select count(*) from book";
        Integer count = jdbcTemplate.queryForObject(sql, Integer.class);
        return count;
    }
```

#### **查询返回对象**

```java
1、场景：查询图书书籍
2、用JdbcTemplate实现查询返回对象
	@Override
	public <T> List<T> query(String sql, @Nullable Object[] args, RowMapper<T> rowMapper) throws DataAccessException {
		return result(query(sql, args, new RowMapperResultSetExtractor<>(rowMapper)));
	}
	An interface used by JdbcTemplate for mapping rows of a ResultSet on a per-row basis. Implementations of this interface perform the actual work of mapping each row to a result object, but don't need to worry about exception handling. SQLExceptions will be caught and handled by the calling JdbcTemplate.
第二个参数：RowMapper,是接口，返回不同类型数据，使用这个接口里面实现类完成数据封装
```

![](.\images\微信截图_20220119092438.png)

```java
 //根据id查询返回对象
    @Override
    public Book findBookInfo(String id) {
        String sql="select * from book where where userId=?";
        //调用方法
        jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<Book>(Book.class), );
        return null;
    }
```

#### 查询返回集合

```java
1、场景：查询图书返回场景
	<T> List<T> query(String sql, RowMapper<T> rowMapper, @Nullable Object... args) throws DataAccessException;

```



### 5、Jdbc操作数据库-批量操作

```java
1、批量操作：操作表里面多条记录
int[] batchUpdate(String sql, List<Object[]> batchArgs) throws DataAccessException;
有两个参数：
第一个参数：sql语句
第二个参数：list集合，添加多条记录数据
 //批量添加
    @Override
    public void batchAddBook(List<Object[]> batchArgs) {
        String sql="insert into book values(?,?,?)";
        int[] ints = jdbcTemplate.batchUpdate(sql, batchArgs);
        System.out.println(Arrays.toString(ints));
    }
测试
 @Test
    public void test06() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);
        List<Object[]> batchArgs = new ArrayList<>();
        Object[] o1={"3","java","a"};
        Object[] o2={"4","java","b"};
        Object[] o3={"5","c","java"};
        batchArgs.add(o1);
        batchArgs.add(o2);
        batchArgs.add(o3);
        //调用批量添加方法
        bookService.batchAdd(batchArgs);
    }
    
/**批量修改**/
    @Test
    public void test07() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);
        List<Object[]> batchArgs = new ArrayList<>();
        Object[] o1={"java","a","3"};
        Object[] o2={"java","b","4"};
        Object[] o3={"mysql","c","5"};
        batchArgs.add(o1);
        batchArgs.add(o2);
        batchArgs.add(o3);
        bookService.batchUpdateBook(batchArgs);
    }
/**批量删除**/
 @Test
    public void test08() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);
        List<Object[]> batchArgs = new ArrayList<>();
        Object[] o1={"3"};
        Object[] o2={"4"};
        batchArgs.add(o1);
        batchArgs.add(o2);
        //调用方法实现批量修改
        bookService.batchDelete(batchArgs);
    }
```

## 5、事务管理

### 1、事务概念

#### 1、什么是事务

事务是数据库操作最基本单元。逻辑上一组操作，要么都成功，如果有一个失败所有操作都失败。

典型场景：`银行转账`

lucy转账100元，给mary

lucy少100，mary多100

#### 2、事务四大特性（ACID）

- 原子性
- 一致性
- 隔离性
- 持久性

### 2、事务操作-搭建事务环境

![](.\images\图6.png)

#### 1、创建数据库表，添加记录

#### 2、创建service，搭建dao,完成对象的创建和注入关系

- service 注入dao,在dao中注入JdbcTemplate,在JdbcTemplate注入DataSource

```java
@Service
public class UserService {
//注入dao
 @Autowired
 private UserDao userDao;
}
@Repository
public class UserDaoImpl implements UserDao{
//注入JdbcTemplate
@Autowired
 private JdbcTemplate jdbcTemplate;
}
```

- 在dao创建两个方法，多钱和少钱的方法，在service创建方法（转账的方法）

```java
@Repository
public class UserDaoImpl implements UserDao{
    //注入JdbcTemplate
   @Autowired
   private JdbcTemplate jdbcTemplate;

    //mary多钱
    @Override
    public void addMoney() {
        String sql="update t_count set money=money-? where username=?";
        int lucy = jdbcTemplate.update(sql, 100, "lucy");


    }
    //lucy少钱
    @Override
    public void reduceMoney() {
        String sql="update t_count set money=money+? where username=?";
        int mary = jdbcTemplate.update(sql, 100, "mary");
    }
}
@Service
public class UserService {

    //注入dao
    @Autowired
    private UserDao userDao;

    //转账的方法
    public void accountMoney(){
        //lucy 少100
        userDao.reduceMoney();
        //mary 多100
        userDao.addMoney();
    }
}
```



#### 3、事务场景引入

`上面代码，如果正常执行没有任何问题，但是如果代码在执行过程中出现异常，有问题`

```java
  //lucy 少100
        userDao.reduceMoney();
/**比如说代码在这行出现问题了 比如断电
Lucy确实少了100，但是mary并没有多**/

 //转账的方法
    public void accountMoney(){
        //lucy 少100
        userDao.reduceMoney();
        //模拟异常
        int i=10/0;
        //mary 多100
        userDao.addMoney();
    }
上面的问题如何解决呢？
使用事务解决

```

#### 4、事务操作过程

```java
//开启事务
    //转账的方法
    public void accountMoney() {
        try {
            //第一步 开启事务
            //第二步进行业务操作
            //lucy 少100
            userDao.reduceMoney();
            //模拟异常
            int i = 10 / 0;
            //mary 多100
            userDao.addMoney();
            //第三步 没有发生异常，提交事务
        } catch (Exception e) {
            //第四步 出现异常，事务回滚
        }
    }

```

### 3、Spring事务管理介绍

#### 1、事务添加到JavaEE三层结构里面Service层（业务逻辑层）

#### 2、在Spring进行事务管理操作（两种方式）

##### 1、编程式事务管理

##### 2、声明式事务管理（推荐使用）

###### 1、基于注解方式

```xml
1、在spring配置文件配置事务管理器

    <!-- 创建事务管理器  -->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <!--   注入数据源     -->
        <property name="dataSource" ref="dataSource"></property>
        
    </bean>
2、在spring配置文件，开启事务注解
a)在Spring配置文件引入名称空间tx
 http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd
b)开启事务注解
 <!--  开启事务注解-->
    <tx:annotation-driven transaction-manager="transactionManager"></tx:annotation-driven>
3、在service类上面（获取service类里面上面）添加事务注解
1）@Transactional,这个注解添加到类上面，也可以添加到方法上面
2）如果把这个注解添加到类上面，这个类里面所有的方法都添加事务
3）如果把这个注解添加到方法上面，为这个方法添加事务。
@Service
@Transactional//既可以加到类上也可以加到注解上
public class UserService {
```

- （**声明式事务管理参数配置**）

```java
1、在service类上面添加注解@Transaction,在这个注解里面可以配置事务相关参数
2、propagation:事务传播行为
3、isolation:事务隔离级别
4、timeout:超时时间
5、readOnly:是否只读
6、rollbackFor:回滚
7、noRollbackFor:不回滚
@Service
@Transactional(propagation=Propagation.REQUIRED)
public class UserService{
   
}    
```

- isolation:事务隔离级别

```java
a)事务有特性称为隔离性，多事务操作之间不会产生影响。不考虑隔离性产生很多问题
b)有三个读问题：脏读、不可重复读、幻读（虚读）
	脏读：一个未提交事务读取到另一个未提交事务的数据
	不可重复读：一个未提交事务读取到另一提交事务修改数据
	幻读：一个未提交事务读取到另一提交事务添加数据
c)解决方案通过设置事务隔离级别，解决读问题
```

![](.\images\事务隔离级别.bmp)

```java
@Service
@Transactional(propagation=Propagation.REQUIRED，isolation=Isolation.REPETABLE_READ)
/**该隔离级别可能存在幻读，但不会出现脏读或者不可重复读**/
如果不设置的话该demo就是默认的
public class UserService{
   
}   
```

- timeout:超时时间

```java
1、事务需要在一定时间内进行提交，如果不提交进行回滚
2、默认值是-1，设置时间以秒单位进行计算
```



- readOnly:是否只读

```java
1、读：查询操作，写：添加修改删除操作
2、readOnly默认值false,表示可以查询，可以添加修改删除操作
3、设置readOnly值是true,设置成true之后，只能查询
```

- rollbackFor:回滚

```java
1、设置出现哪些异常进行事务回滚

```

- noRollbackFor:不回滚

```java
1、设置出现哪些异常不进行事务回滚
```

![](.\images\图8.png)

`@Transaction`相关参数

![](.\images\微信截图_20220119174625.png)

![](.\images\图7.png)

![](.\images\事务传播行为.bmp)

###### 2、基于xml配置文件方式

```xml
1、在spring配置文件中进行配置
第一步 配置事务管理器
 <!-- 1、创建事务管理器  -->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <!--   注入数据源     -->
        <property name="dataSource" ref="dataSource"></property>
    </bean>
```

```xml
第二步 配置通知（事务）
 <!--  配置通知 -->
    <tx:advice id="txadvice">
        <!--  配置事务参数  -->
        <tx:attributes>
            <!-- 指定那种规则的方法上面添加事务 -->
            <tx:method name="accountMoney" propagation="REQUIRED"/>
        <!--<tx:method name="account*"/>-->
        </tx:attributes>
    </tx:advice>

```

```xml
第三步 配置切入点和切面
 <!-- 3、 配置切入点和切面   -->
    <aop:config>
        <!--   配置切入点     -->
        <aop:pointcut id="pt" expression="execution(* com.liang.service.UserService.*(..))"/>
        <!--   配置切面     -->
        <aop:advisor advice-ref="txadvice" pointcut-ref="pt"/>
    </aop:config>
```

###### 3、完全注解声明式事务管理

```java
第一步创建配置类，使用配置类替代xml配置文件
@Configuration //配置类
@ComponentScan(basePackages = "com.liang") //组件扫描
@EnableTransactionManagement // 开启事务
public class TxConfig {
    //创建数据库的连接池
    @Bean
    public DruidDataSource getDruidDataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql:///ssmbuild");
        dataSource.setUsername("root");
        dataSource.setPassword("123456");
        return dataSource;
    }

    //创建JdbcTemplate
    @Bean
    public JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        //注入datasource对象
        jdbcTemplate.setDataSource(dataSource);
        return jdbcTemplate;
    }

    //创建事务管理器
    @Bean
    public DataSourceTransactionManager getDataSourceTransactionManager(DataSource dataSource) {
        DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
        dataSourceTransactionManager.setDataSource(dataSource);
        return dataSourceTransactionManager;
    }
}

```



#### 3、**在Spring进行声明式事务管理，底层使用AOP原理**

#### 4、spring事务管理API

##### 1、提供一个接口，代表事务管理器，这个接口针对不用的框架提供不同的实现类

![](.\images\微信截图_20220119164901.png)



## 6、Spring新功能

### 1、整个框架的代码基于java8

- 通过使用泛型等特性提高可读性
- 对java8提高直接的代码支撑
- 运行时兼容jdk9
- javaEE7 API需要Spring相关的模块支持
- 运行时兼容javaEE8 API
- 取消的包，类，和方法
- 包beans.factory.access
- 包dbc.support.nativejdbc
- 从spring-aspects模块移除了包mock.staticmock,不再提供AnnotationDrivenStaticEntityMockingControl支持
- 许多不建议使用的类和方法在代码库中删除

### spring框架新功能

```java
1、整个Spring5框架的代码基于Java8,运行时兼容JDK9,许多不建议使用的类和方法在代码库中删除
2、有效的方法参数访问基于java8反射增强
3、spring5.0框架自带了通用的日志封装

```

#### spring5.0框架自带了通用的日志封装

```java
a)Spring5已经移除了Log4jConfigListener,官方建议使用Log4j2
b)Spring5框架整合Log4j2的日志
1、第一步：引入jar包
2、第二步创建Log4j2.xml配置文件

```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!--日志级别以及优先级排序: OFF > FATAL > ERROR > WARN > INFO > DEBUG > TRACE > ALL -->
<!--Configuration后面的status用于设置log4j2自身内部的信息输出，可以不设置，当设置成trace时，可以看到log4j2内部各种详细输出-->
<configuration status="INFO">
    <!--先定义所有的appender-->
    <appenders>
        <!--输出日志信息到控制台-->
        <console name="Console" target="SYSTEM_OUT">
            <!--控制日志输出的格式-->
            <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n"/>
        </console>
    </appenders>
    <!--然后定义logger，只有定义了logger并引入的appender，appender才会生效-->
    <!--root：用于指定项目的根日志，如果没有单独指定Logger，则会使用root作为默认的日志输出-->
    <loggers>
        <root level="info">
            <appender-ref ref="Console"/>
        </root>
    </loggers>
</configuration>
```



### 2、Spring5新功能-Nullable注解

#### spring5框架核心容器支持@Nullable注解

- ### @Nullable注解可以使用在方法上面，属性上面，参数上面，表示方法返回可以为空，属性值可以为空，参数值可以为空

```java
注解用在方法上面，方法返回值可以为空
@Nullable
	String getId();
注解使用在方法参数里面
@Override
	public <T> void registerBean(@Nullable String beanName, Class<T> beanClass,
			@Nullable Supplier<T> supplier, BeanDefinitionCustomizer... customizers) {

		this.reader.registerBean(beanClass, beanName, supplier, customizers);
	}
注解使用在属性上面，属性值可以为空
@Nullable
private String bookName;
```

### 3、Spring5核心容器支持函数式风格GenericApplicationContext/AnnotationConfigApplicationContext

```java
public class User {
}
//创建GenericApplicationContext
 /**
     * 函数式风格创建对象，交给spring进行管理GenericApplicationContext
     */
    @Test
    public void testGenericApplicationContext(){
        //1、创建GenericApplicationContext对象
        GenericApplicationContext context = new GenericApplicationContext();
        //2、调用context的方法对象注册
        context.refresh();
       // BeanDefinitionCustomizer
        context.registerBean("user1",User.class, ()->new User());//User::new
//        context.registerBean("user1",User.class, ()->new User());//User::new
        //3、获取在spring注册的对象
//        User user = (User)context.getBean("com.liang.test.User");
        User user = (User)context.getBean("user1");
//        User user = context.getBean(User.class);//通过反射
        System.out.println(user);


    }
```

### 4、Spring5支持整合Junit5

```java
1、整合Junit4
第一步 引入Spring相关针对测试依赖
spring-test-5.2.6.RELEASE.jar
第二步 创建测试类，使用注解方式完成
@RunWith(SpringJUnit4ClassRunner.class)//单元测试框架
@ContextConfiguration("classpath:bean1.xml")//加载配置文件 代替了之前ClassPathXmlApplicationContext
public class JTest4 {
    @Autowired
    private UserService userService;
    @Test
    public void test1(){
        userService.accountMoney();
    }
}
```

#### 1、spring整合Junit5 测试方面的改进

```java
SpringExtension:是Junit多个可扩展API的一个实现，提供了对现存Spring  TestContext Framework的支持，使用
@ExtendWith(SpringExtension.class)注解引用。
@SpringJunitConfig:一个复合注解
@ExtendWith(SpringExtension.class) 来源于Junit Jupit
@ContextConfiguration:来源于Spring TestContext框架
@DisabledIf如果提供的该属性值为true的表达或占位符，信号；注解的测试类或测试方法被禁用
```

```java
spring5整合Junit5
1、第一步：引入Junit5的jar包
2、
@ExtendWith(SpringExtension.class) //来源于Junit Jupit
@ContextConfiguration("classpath:bean1.xml")//加载配置文件 代替了之前ClassPathXmlApplicationContext
public class JTest5 {
    @Autowired
    private UserService userService;

    @Test
    public void test1(){
        userService.accountMoney();
    }
}
3、使用复合的注解代替上面两个注解完成整合
//@ExtendWith(SpringExtension.class) //来源于Junit Jupit
//@ContextConfiguration("classpath:bean1.xml")//加载配置文件 代替了之前ClassPathXmlApplicationContext
@SpringJUnitConfig(locations = "classpath:bean1.xml")
public class JTest5 {
    @Autowired
    private UserService userService;
    @Test
    public void test1(){
        userService.accountMoney();
   }
}
```



### 



### 













## 7、Spring5新特性

#### 1、SpringWebFlux

```java
1、是Spring5添加新的模块，用于web开发的，功能SpringMVC类似的，webflux使用当前一种比较流程响应式编程出现的框架
2、使用传统web框架，比如SpringMVC,这些基于servlet容器，webflux是一种异步非阻塞的框架，异步非阻塞的框架在Servlet3.1以后才支持，核心是基于Reactor的相关API实现的
```

##### 1、解释什么是异步非阻塞

- 异步同步
- 非阻塞和阻塞

**异步和同步针对调用者 上面都是针对对象不一样 调用者发送请求，如果等着对方回应之后才去做其他事情就是同步，如果发送请求之后不等着对方回应就去做其他事情就是异步**

**阻塞和非阻塞针对被调用者** 被调用者收到请求之后，做完请求任务之后才给出反馈就是阻塞 收到请求之后马上给出反馈然后再去做事情就是非阻塞。

##### 2、 webflux特点：

第一非阻塞式：在有限资源下提高系统吞吐量和伸缩性，以Reactor为基础实现响应式编程

第二函数式编程：Spring5框架基于java8,webflux使用java8函数式编程方式实现路由请求

##### 3、与springmvc比较

第一两个框架都可以使用注解方式，都运行在Tomcat等容器中

第二Springmvc采用命令式编程，WebFlux采用异步响应式编程

![](.\images\微信截图_20220117202715.png)

#### 2、响应式编程

##### 1、什么是响应式编程

响应式编程是一种面向数据流和变化传播的编程范式。这意味着可以在编程语言中很方便地表达静态或动态的数据流，而相关的计算模型会自动将变化的值通过数据流进行传播。

##### 2、java8及其之前版本

提供的**观察者模式**两个类**Observer**和**Observable**

```java
public class ObserverDemo extends Observable {
    public static void main(String[] args) {
        ObserverDemo observer = new ObserverDemo();
        //添加观察者
        observer.addObserver((o, arg) -> {
            System.out.println("发生变化");
        });

        observer.addObserver((o, arg) -> {
            System.out.println("手动被观察者通知，准备改变");
        });
        /**
         * 监控
         */
        observer.setChanged();//监控到数据得到变化
        observer.notifyObservers();//通知
    }

}
```

##### 3、响应式编程（Reactor实现）

- 响应式编程操作中，满足Reactor规范
- Reactor有两个核心类，Mono和Flux,这两个类实现接口Publisher，提供丰富操作符。Flux对象实现发布者，返回n个元素；Mono实现发布者，返回0或者1个元素。
- Flux和Mono都是数据流的发布者，使用Flux个Mono都可以发出三种数据信号：元素值，错误信号，完成信号，`错误信号`和`完成信号`都代表终止信号，终止信号用于告诉订阅者数据流结束了，错误信号终止数据流，同时把错误的信息传递给我们的订阅者。

##### 4、代码演示flux和Mono

第一步引入依赖

```xml
 <dependency>
            <groupId>io.projectreactor</groupId>
            <artifactId>reactor-core</artifactId>
            <version>3.1.5.RELEASE</version>
        </dependency>
```

第二步编程代码

```java
public class TestReactor {
    public static void main(String[] args) {
       //just方法直接声明
        Flux.just(1,2,3,4);
        Mono.just(1);
        //其他方法
        Integer[] array={1,2,3,4};
        Flux.fromArray(array);

        List<Integer> list= Arrays.asList(array);
        Flux.fromIterable(list);
        Stream<Integer> stream=list.stream();
        Flux.fromStream(stream);
    }
}

```

##### 5、三种信号特点

`错误信号`和`完成信号`都是终止信号，不能共存的。

如果没有发送任何元素值，而是直接发送错误或者完成信号，表示空数据流

如果没有错误信号，没有完成信号，表示是无限数据流

##### 6、调用just或者其他方法只是声明数据流，数据流并没有发出，只有进行订阅之后才会触发数据流，不订阅什么都不会发生的。

```java
 //just方法直接声明
        Flux.just(1,2,3,4).subscribe(System.out::println);
        Mono.just(1).subscribe(System.out::print);
```

##### 7、操作符 map 和 flatmap

对数据流进行一道道操作，成为操作符，比如工厂的流水线

第一: **map元素映射为新元素**

![](.\images\微信截图_20220117224420.png)

第二:**flatMap元素映射为流**

把每个元素转换流，把转换成之后多个流合并成大流

![](.\images\微信截图_20220117224946.png)

### 3、WebFlux执行流程和核心API

SpringWebFlux基于Reactor,默认容器是Netty,Netty是高性能，Nio框架，异步非阻塞的框架，

#### 1、Netty

- BIO:

  ![](.\images\微信截图_20220118090505.png)

- NIO 

  netty用的这个架构

  ![](.\images\微信截图_20220118090910.png)

#### 2、SpringWebFlux执行过程和SpringMVC相似的

- SpringWebFlux**核心控制器**`DispatcherHandler`,实现接口`WebHandler`

```java
public interface WebHandler {
    Mono<Void> handle(ServerWebExchange exchange);
}
实现类DispatcherHandler
 public Mono<Void> handle(ServerWebExchange exchange) { //放http请求相应信息
        if (this.handlerMappings == null) {
            return this.createNotFoundError();
        } else {
            return CorsUtils.isPreFlightRequest(exchange.getRequest()) ? this.handlePreFlight(exchange) : Flux.fromIterable(this.handlerMappings).concatMap((mapping) -> {
                return mapping.getHandler(exchange);//根据请求地址获取对应的mapping
            }).next().switchIfEmpty(this.createNotFoundError()).flatMap((handler) -> {
                return this.invokeHandler(exchange, handler);//调用具体的业务方法
            }).flatMap((result) -> {
                return this.handleResult(exchange, result);//处理结果返回
            });
        }
    }
```

- SpringWebFlux里面DispatcherHandler ,负责请求的处理 还有其他的组件

```java
1、HanddlerMapping:请求查询到处理的方法
2、HandlerAdapter：真正负责请求处理
3、HandlerResultHandler：响应结果处理
```

- SpringWebflux实现函数式 编程，两个接口：RouterFunction(路由处理)和HandlerFunction（处理函数）

![](.\images\微信截图_20220118093847.png)

### 4、SpringWebFlux（基于注解编程模型）

SpringWebFlux实现方式有两种：**注解编程模型**和**函数式编程模型**

使用注解编程模型方式，和之前SpringMVC使用相似的，只需要把相关依赖配置到项目中，SpringBoot自动配置相关运行容器，默认情况下使用Netty服务器

#### 1、第一步：创建SpringBoot工程，引入Webflux依赖

```xml
  <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-webflux</artifactId>
        </dependency>
```

#### 2、第二步：配置启动的端口号

```yml
server.port=8083
```

#### 3、第三步：创建包和相关类

```java
public interface UserService {
    //根据id查询用户
    Mono<User> getUserById(int id);

    //查询所有用户
    Flux<User> getAllUser();

    //添加用户
    Mono<Void> saveUserInfo(Mono<User> user);

}
```

#### 4、接口的实现类

```java
public class UserServiceImpl implements UserService {

    //创建map集合存储数据
    private final Map<Integer, User> users = new HashMap<>();

    public UserServiceImpl() {
        this.users.put(1, new User("lucy", "女", 20));
        this.users.put(2, new User("mary", "女", 30));
        this.users.put(3, new User("jack", "女", 50));

    }

    //根据id查询
    @Override
    public Mono<User> getUserById(int id) {
        return Mono.justOrEmpty(this.users.get(id));
    }

    //查询多个用户
    @Override
    public Flux<User> getAllUser() {
        return Flux.fromIterable(this.users.values());
    }

    //添加一个用户
    @Override
    public Mono<Void> saveUserInfo(Mono<User> userMono) {
        return userMono.doOnNext(person -> {
            //向map中集合里面放值
            int id = users.size() + 1;
            users.put(id, person);
        }).thenEmpty(Mono.empty()); //
    }
}

```

#### 5、创建controller

```java
@RestController
public class UserController {
    //注入Service
    @Autowired
    private UserService userService;

    //id 查询
    @GetMapping("/user/{id}")
    public Mono<User> getUserId(@PathVariable int id) {
        return userService.getUserById(id);
    }
    //查询所有
    @GetMapping("/user")
    public Flux<User> getUsers(){
        return userService.getAllUser();

    }
    //添加
    @PostMapping("/saveUser")
    public Mono<Void> saveUser(@RequestBody User user){
        Mono<User> userMono = Mono.just(user);
       return userService.saveUserInfo(userMono);
    }
}
```

#### 6、最终说明

```
SpringMvc方式出现，同步阻塞的方式，基于SpringMVC+Servlet+Tomcat
SpringWebFlux方式实现，异步非阻塞的方式，基于SpringWebFlux+Reactor+Netty
```

### 5、SpringWebFlux(基于函数式编程)

### 1过程

1. 在使用函数式编程模型操作的时候，需要自己初始化服务器
2. 基于函数式编程模型时候，有两个核心接口：RouterFunction（实现路由功能，请求转发给对应的handler）和HandlerFunction（）(处理请求生成响应的函数)。核心任务定义这两个函数式接口的实现并且启动需要的服务器
3. springWebflux请求和响应不再是ServletRequest和ServletResponse,而是ServerRequest和ServerResponse

### 2步骤

#### 1、把注解编程模型工程复制一份

#### 2、创建handler(具体实现方法)

```java
public class UserHandler {
    private final UserService userService;

    public UserHandler(UserService userService) {
        this.userService = userService;
    }

    //根据id 查询
    public Mono<ServerResponse> getUserById(ServerRequest request) {
        //获取id值
        int userId = Integer.valueOf(request.pathVariable("id"));
        //空值的一个处理
        Mono<ServerResponse> notFound = ServerResponse.notFound().build();
        //调用service方法得到数据
        Mono<User> userMono = this.userService.getUserById(userId);
        //把userMono进行转换返回

        //使用Reactor操作符flatMap
        return
                userMono
                        .flatMap(person -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                                .body(fromObject(person)))
                        .switchIfEmpty(notFound);


    }

    //查询所有
    public Mono<ServerResponse> getAllUsers() {
        //调用service得到结果
        Flux<User> uesrs = this.userService.getAllUser();
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(uesrs, User.class);

    }

    //添加
    public Mono<ServerResponse> saveUser(ServerRequest request) {
        //得到user对象
        Mono<User> userMono = request.bodyToMono(User.class);
        return ServerResponse.ok().build(this.userService.saveUserInfo(userMono));

    }
}
```

#### 3、初始化服务器，编写Router

![](.\images\微信截图_20220118140917.png)

```java
public class Server {
    //1、创建Router路由
    public RouterFunction<ServerResponse> routingFunction() {
        UserService userService = new UserServiceImpl();
        UserHandler handler = new UserHandler(userService);
        //设置路由
        return
                RouterFunctions.route(
                        GET("/users/{id}").and(accept(APPLICATION_JSON)), handler::getUserById)
                        .andRoute(
                                GET("/users").and(accept(APPLICATION_JSON)),handler::getAllUsers);
    }
}

```

#### 4、创建服务器完成

```java
  //3、创建服务器完成适配
    public void createReactorServer(){
        //路由和handler适配
        RouterFunction<ServerResponse> route = routingFunction();
        HttpHandler httpHandler = toHttpHandler(route);
        ReactorHttpHandlerAdapter adapter = new ReactorHttpHandlerAdapter(httpHandler);

        //创建服务器
        HttpServer httpServer=HttpServer.create();
        httpServer.handle(adapter).bindNow();

    }
```

#### 5、调用

```java
 public static void main(String[] args) throws Exception {
        Server server = new Server();
        server.createReactorServer();
        System.out.println("enter to exit");
        System.in.read();

    }
```

#### 6、测试

### 3、使用WebClient调用

```java
public class Client {
    public static void main(String[] args) {
        //调用服务器地址
        WebClient webclient =WebClient.create("http://127.0.0.1:49962");
        //根据id查询
        String id="1";
        User block = webclient.get()
                .uri("/users/{id}", id)
                .accept(MediaType.APPLICATION_JSON).retrieve()
                .bodyToMono(User.class)
                .block();
        System.out.println(block.toString());

        //查询所有
        Flux<User> results=webclient.get()
                .uri("/users")
                .accept(MediaType.APPLICATION_JSON).retrieve()
                .bodyToFlux(User.class);
            results.map(stu->stu.getName())
                    .buffer()
                    .doOnNext(System.out::println)
                    .blockFirst();//block相当于订阅的部分 不订阅是取不出来的

    }
}
```



### 











