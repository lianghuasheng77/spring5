package com.liang.test;

import com.liang.config.TxConfig;
import com.liang.service.UserService;
import org.junit.Test;
import org.springframework.beans.factory.config.BeanDefinitionCustomizer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/19/15:52
 * @Description:
 */
public class TestAccount {
    /**
     * 测试lucy与mary 转账业务
     */
    @Test
    public void testAccount() {


        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
        UserService userService = context.getBean("userService", UserService.class);
        userService.accountMoney();
    }

    /**
     * 测试声明式事务管理
     */
    @Test
    public void testAccountXml() {

        ApplicationContext context = new ClassPathXmlApplicationContext("bean2.xml");
        UserService userService = context.getBean("userService", UserService.class);
        userService.accountMoney();
    }

    /**
     * 测试声明式事务完全注解
     *
     */
    @Test
    public void testAccountAnno() {
        ApplicationContext context = new AnnotationConfigApplicationContext(TxConfig.class);
        UserService userService = context.getBean("userService", UserService.class);
        userService.accountMoney();

    }
    /**
     * 函数式风格创建对象，交给spring进行管理GenericApplicationContext
     */
    @Test
    public void testGenericApplicationContext(){
        //1、创建GenericApplicationContext对象
        GenericApplicationContext context = new GenericApplicationContext();
        //2、调用context的方法对象注册
        context.refresh();
       // BeanDefinitionCustomizer
        context.registerBean("user1",User.class, ()->new User());//User::new
//        context.registerBean("user1",User.class, ()->new User());//User::new
        //3、获取在spring注册的对象
//        User user = (User)context.getBean("com.liang.test.User");
        User user = (User)context.getBean("user1");
//        User user = context.getBean(User.class);//通过反射
        System.out.println(user);


    }
}
