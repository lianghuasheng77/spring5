package com.liang.service;

import com.liang.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/19/15:04
 * @Description:
 */
@Service
//@Transactional(propagation = Propagation.REQUIRED,noRollbackFor=ClassNotFoundException.class)//既可以加到类上也可以加到注解上
@Transactional
public class UserService {

    //注入dao
    @Autowired
    private UserDao userDao;

    //转账的方法
    public void accountMoney() {

//        try {
            //第一步 开启事务
            //第二步进行业务操作
            //lucy 少100
            userDao.reduceMoney();
            //模拟异常
            int i = 10 / 0;
            //mary 多100
            userDao.addMoney();
            //第三步 没有发生异常，提交事务
//        } catch (Exception e) {
//            //第四步 出现异常，事务回滚
//        }


    }
}
