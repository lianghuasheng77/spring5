package com.liang.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/19/15:05
 * @Description:
 */
@Repository
public class UserDaoImpl implements UserDao{
    //注入JdbcTemplate
   @Autowired
   private JdbcTemplate jdbcTemplate;

    //mary多钱
    @Override
    public void addMoney() {
        String sql="update t_account set money=money-? where username=?";
        int lucy = jdbcTemplate.update(sql, 100, "lucy");


    }
    //lucy少钱
    @Override
    public void reduceMoney() {
        String sql="update t_account set money=money+? where username=?";
        int mary = jdbcTemplate.update(sql, 100, "mary");
    }
}
