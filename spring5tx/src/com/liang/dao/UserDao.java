package com.liang.dao;

/**
 * Created with Intellij IDEA
 *
 * @Auther: liangjy
 * @Date: 2022/01/19/15:05
 * @Description:
 */
public interface UserDao {
    //多钱的方法
    public void addMoney();
    //少钱的方法
    public void reduceMoney();
}
